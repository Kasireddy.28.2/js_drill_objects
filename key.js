function keys(obj){
    if(typeof(obj)==="object"){
        let keys=[];
        for(let key in obj){
            if(obj.hasOwnProperty(key)){
                keys.push(key);
            }else{
                return `key is not in the object`
            };
        };
        return keys;
    }else{
        return undefined;
    }
};

module.exports=keys;