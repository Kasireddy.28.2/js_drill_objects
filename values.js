function values(obj){
    if(typeof(obj)==="object"){
        let values=[];
        for(let key in obj){
            if(obj.hasOwnProperty(key)){
                values.push(obj[key]);
            }else{
                return `key is not in the object`
            }
        }
        return values;
    }else{
        return undefined
    }
    
};

module.exports=values;