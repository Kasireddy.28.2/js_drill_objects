function invert(obj){
    if(typeof(obj)==="object"){
        let invertObj={};
        for(let key in obj){
            if(obj.hasOwnProperty(key)){
                invertObj[obj[key]]=key;
            }else{
                return `key is not in the object`;
            }
        }
        return invertObj;
    }else{
        return undefined;
    }
};

module.exports=invert;