function defaults(obj1,obj2){
    if(typeof(obj1)==="object" && typeof(obj2)==="object"){
        for(let key1 in obj1){
            for(let key2 in obj2){
                if(key1===key2 && (obj1[key1]===undefined || obj1[key1]===null)){
                    obj1[key1]=obj2[key2];
                };
            };
        };
        return obj1;
    }else{
        return undefined;
    }
    
};

module.exports=defaults;