function pairs(obj){
    if(typeof(obj)==="object"){
        let array=[];
        for(let key in obj){
            if(obj.hasOwnProperty(key)){
                array.push([key,obj[key]]);
            }else{
                return `key is not in the object`
            }
        }
        return array;
    }else{
        return undefined
    }
    
};

module.exports=pairs;