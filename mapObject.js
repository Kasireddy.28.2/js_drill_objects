function mapObject(obj,cb){
    if(typeof(obj)==="object"){
        let newObj={};
        for(let key in obj){
            if(obj.hasOwnProperty(key)){
                newObj[key]=cb(obj[key],key,obj);
            }else{
                return `key is not in the object`
            }
        };
        return newObj;
    
    }else{
        return undefined
    }
    

};

module.exports=mapObject;